<?php echo $this->Form->create('User', array('class' => 'form-signin', 'inputDefaults' => array('div' => false, 'label' => false))); ?>
<h2 class="form-signin-heading"><?php echo __('Please sign in'); ?></h2>
<?php echo $this->TB->input('username', array('class' => 'span3', 'label' => false, 'placeholder' => 'Username', 'prepend' => '<i class="icon-user"></i>')); ?>
<?php echo $this->TB->input('password', array('class' => 'span3', 'label' => false, 'placeholder' => 'Password', 'prepend' => '<i class="icon-lock"></i>')); ?>
<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>